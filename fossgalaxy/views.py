from django.shortcuts import render

from fg_news.models import Announcement

def index(request):
	"""Index page welcoming people to the site"""
	context_dict = {}
	context_dict['announcements'] = Announcement.objects.all()[:5]
	context_dict['welcome_text'] = "Donec sit amet tellus dui. In hac habitasse platea dictumst. Aenean eu justo et ante lobortis suscipit. Suspendisse potenti. Fusce convallis lectus ut ullamcorper sodales. Donec at porta eros. Ut ornare sagittis tincidunt."
	context_dict["features"] = [
			{"title": "Community", "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed imperdiet mauris. Vestibulum scelerisque at.", "icon": "users"},
			{"title": "Code", "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras condimentum, ante in tempus viverra, lacus.", "icon": "code"},
			{"title": "Services", "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin leo augue, pellentesque sed elit in.", "icon": "wrench"},
		]
	return render(request, 'index.html', context_dict)
