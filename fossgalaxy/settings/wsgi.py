##
# WSGI Production settings
#
# Unless you're adding new stuff/fixing something, you shouldn't need to change this.
#
# The following enviroment variables exist to make life easier:
# FG_SSL_ONLY -> set to "True" to enable SSL only mode
# FG_SECRET_KEY -> set to set the secret key for django
# FG_HOST_NAME -> set the host this system should care about
# FG_SITE_ID -> set the django site ID we're responsible for serving
##

import os
from fossgalaxy.settings.defaults import *

# Values popluated fron env variables
ALLOWED_HOSTS = os.environ.get('FG_HOST_NAME','.fossgalaxy.com').split(",")
SITE_ID = int(os.environ.get('FG_SITE_ID','1'))
FG_SSL_ONLY = os.environ.get('FG_SSL_ONLY', 'False') == "True"
SECRET_KEY = os.environ.get('FG_SECRET_KEY', 'SET ME PLEASE')

DEBUG = False # Disable debug mode

# Set postgres fields based on env
DATABASES['default']['NAME'] = os.environ.get('FG_DB_NAME', 'postgres')
DATABASES['default']['USER'] = os.environ.get('FG_DB_USER', 'postgres')
DATABASES['default']['PASSWORD'] = os.environ.get('FG_DB_PASS', '')
DATABASES['default']['HOST'] = os.environ.get('FG_DB_HOST', 'db')


##
# Security Section
#
# Stuff designed to protect the users from attacks
##
#Django secret key
if SECRET_KEY == 'SET ME PLEASE':
	raise ValueError('FG_SECRET_KEY has not been set, abort!')

# Stop browsers doing stuff
SECURE_CONTENT_TYPE_NOSNIFF = True # tell browsers we don't lie about MIME types
SECURE_BROWSER_XSS_FILTER = True # ask the browser to help us prevent attacks
CSRF_COOKIE_HTTPONLY = True # stop JS accessing CSRF token
X_FRAME_OPTIONS = 'DENY' # Don't allow embedding in frames

# SSL related
SECURE_HSTS_SECONDS = 3600 if FG_SSL_ONLY else 0 # number of seconds for browsers to only permit ssl connections for
SECURE_HSTS_INCLUDE_SUBDOMAINS = FG_SSL_ONLY # Only permit SSL connections to subdomains to
SECURE_SSL_REDIRECT = FG_SSL_ONLY # redirect http to https
SESSION_COOKIE_SECURE = FG_SSL_ONLY # only send session cookies over https
CSRF_COOKIE_SECURE = FG_SSL_ONLY # only send CSRF cookies over https
