##
# WSGI Production settings
#
# Unless you're adding new stuff/fixing something, you shouldn't need to change this.
#
# The following enviroment variables exist to make life easier:
# FG_SSL_ONLY -> set to "True" to enable SSL only mode
# FG_SECRET_KEY -> set to set the secret key for django
# FG_HOST_NAME -> set the host this system should care about
# FG_SITE_ID -> set the django site ID we're responsible for serving
##

import os
from fossgalaxy.settings.defaults import *

# Set values to their dev values
ALLOWED_HOSTS = ("localhost",)
SITE_ID = 1
FG_SSL_ONLY = True
SECRET_KEY = 'LOCAL-DEV-SERVER'

DEBUG = True # Disable debug mode

##
# development database
##
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
