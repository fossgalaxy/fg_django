from django.shortcuts import render
from django.views.generic.list import ListView

from fg_news.models import Announcement, Category

# Create your views here.
class AnnouncementListView(ListView):
    """ """
    model = Announcement
    paginate_by = 10
    context_object_name = "announcements"

    def get_context_data(self, **kwargs):
        context = super(AnnouncementListView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        return context

class AnnouncementListView(ListView):
    """ """
    model = Announcement
    paginate_by = 10
    context_object_name = "announcements"

    def get_queryset(self):
        if 'category' in self.kwargs:
            slug = self.kwargs['category']
            return Announcement.objects.filter(category__slug=slug)
        else:
            return Announcement.objects.all()

    def get_context_data(self, **kwargs):
        context = super(AnnouncementListView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        context['selected_category'] = self.kwargs['category'] if 'category' in self.kwargs else None
        return context
