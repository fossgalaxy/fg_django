from django.contrib import admin

from fg_news.models import Category, Announcement

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('__str__','slug', 'icon')

@admin.register(Announcement)
class AnnouncementAdmin(admin.ModelAdmin):
    list_display = ('__str__','category','date')
    list_filter = ('category', 'date')
