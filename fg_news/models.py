from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models
from django.utils import timezone

@python_2_unicode_compatible
class Category(models.Model):
	"""A way of grouping for announcements for filtering"""
	title = models.CharField(max_length=120)
	slug = models.SlugField(primary_key=True)
	icon = models.CharField(max_length=50)

	def __str__(self):
		return self.title

@python_2_unicode_compatible
class Announcement(models.Model):
	"""A short piece of infomation for the homepage"""
	title = models.CharField(max_length=120)
	category = models.ForeignKey(Category)
	link = models.URLField()
	media = models.URLField(blank=True, null=True)
	body = models.TextField()
	date = models.DateTimeField(default=timezone.now)
	created_date = models.DateTimeField(auto_now_add=True)
	modified_date = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.title

	class Meta:
		get_latest_by = "date"
		ordering = ['-date']
