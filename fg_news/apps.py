from django.apps import AppConfig


class FgNewsConfig(AppConfig):
    name = 'fg_news'
